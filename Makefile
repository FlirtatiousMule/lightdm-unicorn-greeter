INSTALL_PATH=/usr/local
CONFIG_PATH=/etc
PKG_PREFIX=

lightdm-lightdm-wayland-greeter.conf: lightdm-wayland-greeter.conf.base
	sed -e "s|INSTALL_PATH|$(INSTALL_PATH)|" lightdm-wayland-greeter.conf.base > lightdm-wayland-greeter.conf

clean:
	rm lightdm-wayland-greeter.conf

install: lightdm-wayland-greeter.conf
	install -D -m 644 -t $(PKG_PREFIX)$(CONFIG_PATH)/lightdm/ lightdm-wayland-greeter.conf
	install -D -m 755 -t $(PKG_PREFIX)$(INSTALL_PATH)/bin lightdm-wayland-greeter.py
	install -D -m 644 -t $(PKG_PREFIX)$(INSTALL_PATH)/share/lightdm/greeters lightdm-wayland-greeter.desktop lightdm-wayland-greeter-x11.desktop
	install -D -m 644 -t $(PKG_PREFIX)$(INSTALL_PATH)/share/lightdm-wayland-greeter lightdm-wayland-greeter.ui
	install -D -m 644 -t $(PKG_PREFIX)$(INSTALL_PATH)/share/lightdm-wayland-greeter/img img/*

uninstall:
	rm $(INSTALL_PATH)/bin/lightdm-wayland-greeter.py
	rm -r $(INSTALL_PATH)/share/lightdm-wayland-greeter/
	rm $(INSTALL_PATH)/share/lightdm/greeters/lightdm-wayland-greeter.desktop
	rm $(INSTALL_PATH)/share/lightdm/greeters/lightdm-wayland-greeter-x11.desktop
	rm $(CONFIG_PATH)/lightdm/lightdm-wayland-greeter.conf

